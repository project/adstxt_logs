CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Use this module along with adstxt module. This module allows you to check
the specific user who has modified ads.txt file. The Module allows you to
download the ads.txt file from a list which shows you the previous details
of ads.txt file.

The Module allows you to view/restore the previous versions of ads.txt.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/adstxt_logs

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/adstxt_logs

REQUIREMENTS
------------

This module requires the following module:

* AdsTxt (https://www.drupal.org/project/adstxt)


INSTALLATION
------------

See http://drupal.org/getting-started/install-contrib for instructions on
how to install or update Drupal modules.

Once you have the AdsTxtlogs modules installed, just check for the listing
by admin/config->System->AdsTxt (admin/config/system/adstxt)

* Remove ads.txt file if you have added at the Drupal root earlier.

CONFIGURATION
-------------

* Configure user permissions in Administration » People » Permissions:

 - Administer ads.txt
   Users in roles with the "Administer ads.txt" permission will edit ads.txt.


MAINTAINERS
-----------

Current maintainers:
* Priyanka Attarde (priyanka.attarde) - https://www.drupal.org/u/priyankaattarde
